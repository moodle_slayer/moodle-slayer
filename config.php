<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'usms';
$CFG->dbpass    = 'Usms2@19';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '/var/run/mysqld/mysqld.sock',
  'dbcollation' => 'utf8_unicode_ci',
);
//$CFG->auth="manual";
$CFG->wwwroot   = 'http://196.200.184.27/moodle';
$CFG->dataroot  = '/var/www/moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!


//ini_set ('display_errors', 'on'); ini_set ('log_errors', 'on'); ini_set ('display_startup_errors', 'on'); ini_set ('error_reporting', E_ALL);